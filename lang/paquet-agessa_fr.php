<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'agessa_description' => 'Pré-remplit le bordereau déclaratif Agessa et génére le pdf.',
	'agessa_nom' => 'Agessa',
	'agessa_slogan' => 'Pré-remplit le bordereau déclaratif Agessa et génére le pdf.',
);