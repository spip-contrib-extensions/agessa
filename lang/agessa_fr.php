<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C 
	'cfg_titre_parametrages' => 'Paramètres de la déclaration Agessa',
	'cfg_taux' => 'Taux en vigueur',
	'cfg_taux_explication' => 'Indiquer les taux en vigueur (exemple 0.011 = 1.1 %)',
	'cfg_taux_precompte' => 'Cotisations / contributions sociales et formation professionnelle',
	'cfg_taux_csg' => 'Cotisation Sociale Généralisée',
	'cfg_taux_rds' => 'Cotisation au Remboursement de la Dette Sociale',
	'cfg_taux_formation' => 'Cotisation auteur formation professionnelle',
	'cfg_auteur' => 'Auteur',
	'cfg_auteur_explication' => 'Renseignements sur l\'auteur et son activité',
	'cfg_insee' => 'N° Sécurité sociale',
	'cfg_sexe' => 'Civilité',
	'cfg_nom' => 'Nom',
	'cfg_prenom' => 'Prénom',
	'cfg_numero' => 'N°',
	'cfg_rue' => 'Rue',
	'cfg_cp' => 'CP',
	'cfg_ville' => 'Ville',
	'cfg_pays' => 'Pays',
	'cfg_activite' => 'Nature de l\'activité de l\'auteur',
	'cfg_taux_diffuseur' => 'Contributeur diffuseur',

	// E
	'erreur_montant_da' => 'Montant incorrect', 

	// T
	'titre_agessa' => 'Agessa',
	'titre_page_configurer_agessa' => 'Agessa',

	// M
	'montant_da' => 'Montant brut HT des droits d\'auteur',

	// P
	'pdf_cree' => 'Votre pdf a été générée avec succès<br /><a href="@pdf_path@" target="_blank">Télécharger le pdf @pdf_nom@</a>',
);